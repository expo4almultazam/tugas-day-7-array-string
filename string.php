<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>contoh soal</h1>
    <?php
        $kalimat1 = "PHP is never old";
        echo "kalimat pertama : ". $kalimat1. "<br>";
        echo "jumlah karakter kalimat pertama : " . strlen($kalimat1). "<br>";
        echo "jumlah kata : " . str_word_count($kalimat1) . "<br>";


        echo "<h3>contoh ke 2</h3>";
        $string2 = "welcome to the jungle";
        echo "kalimat kedua : " .  $string2. "<br>";
        echo "kata pertama : " . substr($string2,0,7) . "<br>";
        echo "kata kedua : " . substr($string2,8,2). "<br>";
        echo "kata ketiga : " . substr($string2,11,3). "<br>";
        echo "kata keempat : " . substr($string2,15,6). "<br>";

        echo "<h3>contoh 3</h3>";
        $string3 = "PHP is old but Good!";
        echo "kalimat ketiga : " . $string3. "<br>";
        echo "ganti kalimat ke 3 : " . str_replace("Good","Awesome", $string3); 
    ?>
</body>
</html>